<cfsetting requestTimeOut = "1200">

<cfinclude template="QueryToCSV.cfm">
<cfset maxRows = 100000>

<!--- RecoveryPACS StartDate = 9/26/2001 --->
<!--- RecoveryPACS EndDate = 9/26/2001 --->
<cfset TrueStartDate = createDate(2001,9,26)>
<cfset TrueEndDate = createDate(2002,12,31)>
  
<cfset StartDate = #DateFormat(TrueStartDate,'yyyy-mm-dd')#>
<cfset EndDate = #DateFormat(TrueEndDate,'yyyy-mm-dd')#>
  
<cfset excludedProcedures = "">
<cfset excludedProcedures = listAppend(excludedProcedures,"'6719'")> <!--- Addl PET Isotope --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6717'")> <!--- Addl PET Area --->
  
<cfset excludedProcedures = listAppend(excludedProcedures,"'6271'")> <!--- MR After Hours --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6679'")> <!--- MR Add'l Area --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6675'")> <!--- MR Contrast Code --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6680'")> <!--- MR RT Setup Scan --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6667'")> <!--- MR Add'l Area (Eq) --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6668'")> <!--- MR Contrast (Eq) --->
  
<cfset excludedProcedures = listAppend(excludedProcedures,"'6921'")> <!--- Level 1 Intervention --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6922'")> <!--- Level 2 Intervention --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6923'")> <!--- Level 3 Intervention --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6904'")> <!--- US Intervention - 15 min --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6904'")> <!--- US Intervention - 15 min --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6927'")> <!--- Complex --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6610'")> <!--- CT Addl Area --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6272'")> <!--- CT After Hours --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6615'")> <!--- RT Setup Scan --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6621'")> <!--- CT Guided Inj / asp --->

<cfset excludedProcedures = listAppend(excludedProcedures,"'6901'")> <!--- SA Ultrasound After Hours --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6925'")> <!--- US Guided Cysto --->

<cfset excludedProcedures = listAppend(excludedProcedures,"'6202'")> <!--- Addl Images --->
<cfset excludedProcedures = listAppend(excludedProcedures,"'6510'")> <!--- Copy Films --->
  
<cfset excludedProcedures = listAppend(excludedProcedures,"'6750'")> <!--- Nucmed Injection Fee --->  
<cfset excludedProcedures = listAppend(excludedProcedures,"'6763'")> <!--- Nucmed Additional Area --->

<cfset ExportToPSV = true>

<cfscript> 
    ///We need an absolute path, so get the current directory path. 
    theFile=GetDirectoryFromPath(GetCurrentTemplatePath()) & "VMACSExport.xls"; 
</cfscript>  

<!--- PHYSICIAN --->
PHYSICIAN - <cfflush>
<cfquery datasource="VMACS-Prod" name="qDoctors" maxrows="#maxRows#">      
  SELECT DISTINCT VMTH_Person.IEN as PhysicianId, 'SYSTEM' as IssuerOfPhysicianId, Last_Name as LastName, UCase(left(vmth_person.name, len(vmth_person.Name)-len(Last_Name)-1)) as FirstName, 
  CASE
        WHEN content IS NOT NULL THEN content
        WHEN Email_forward IS NOT NULL THEN Email_forward
        WHEN Email_Address IS NOT NULL THEN {fn CONCAT(Email_Address,'@vmth.ucdavis.edu')}
    END as Email
  from VMACS.Vmth_Person left outer JOIN VMACS.Contact on contact.User_ID = vmth_person.IEN AND contact_type_id = 1
  inner join VMACS.Acct_Doctor on Acct_Doctor.VMTH_Person = VMTH_Person.IEN
  WHERE VMTH_Person.IEN != 9399
  order by last_name
</cfquery>
  
<cfset queryAddRow(qDoctors)>
    <cfset querySetCell(qDoctors, "PhysicianId", 0)>
    <cfset querySetCell(qDoctors, "IssuerOfPhysicianId", "SYSTEM")>
    <cfset querySetCell(qDoctors, "LastName", "Unknown")>
    <cfset querySetCell(qDoctors, "FirstName", "Unknown")>
    <cfset querySetCell(qDoctors, "Email", "unknown@ucdavis.edu")>

<cfloop query="qDoctors">
  <cfif len(qDoctors.FirstName) eq 0>
    Error with Doctor Export <cfoutput>#qDoctors.PhysicianId# - #qDoctors.LastName#</cfoutput> - Zero length first name
  </cfif>
</cfloop>

<cfif ExportToPSV>
  <cfset PhysicianPSV = QueryToCSV(qDoctors,"PhysicianId,IssuerOfPhysicianId,LastName,FirstName,Email",True,"|")>
    <cffile action="write" file="#expandPath("./doctors.csv")#" output="#left(PhysicianPSV,len(PhysicianPSV)-2)#" nameconflict="overwrite" addnewline="false">    
<cfelse>
  <cfscript>   
    //Create a new Excel spreadsheet object and add the query data. 
    theSpreadsheet = SpreadsheetNew("Doctors"); 
    SpreadsheetAddRow(theSpreadsheet,"PhysicianId,IssuerOfPhysicianId,LastName,FirstName,Email");
    SpreadsheetAddRows(theSpreadsheet,qDoctors); 
  </cfscript>
</cfif> 
DONE<cfflush>
      
<!--- SERVICE REQUEST --->
<br>ServiceRequest - <cfflush>
      
<cfset patientList = ''>
<cfset q = queryNew("PatientId,IssuerOfPatientID,LastName,FirstName,MiddleName,MaidenName,Species,Breed,Sex,DateOfBirth","integer,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar")>
      
<cfset qServiceRequest = queryNew("PatientId,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,OrderPriority,OrderCreationDateTime,Modality, OrderingClinician,OrderingService","integer,varchar,varchar,varchar,varchar,varchar,varchar,integer,varchar")>

<cfset qRequestedProcedure = queryNew("PatientId,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,ProcedureCode,ProcedureDescription,AccessionNumber,RequestedProcedureID,ScheduledProcedureStepID,StudyStatus,ScheduledStudyDateTime,Modality",
       "integer,varchar,varchar,varchar,decimal,varchar,varchar,varchar,varchar,varchar,varchar,varchar")>      

<cfset qAttachment = queryNew("PatientId,IssuerOfPatientId,Type,Code,Content","integer,varchar,varchar,varchar,varchar")>

<cfset modalityList = 'La_Radiography^LAR^DX,Magnetic_Resonance_Imaging^MRI^DX,Ultrasonography^SAU^US,Sa_Radiography^SAR^DX,CT_Scanning^CT,Equine_Ultrasound^EQU^US,Equine_Magnetic_Resonance_Imaging^EQM^MR,Nuclear_Medicine^NM^NM'>   
<cfloop list="#modalityList#" index="currentModality">
  

  <cfquery datasource="VMACS-Prod" name="qImg" maxrows="#maxRows#">
    SELECT DISTINCT PatientNbr, Request, Date_Of_Request, '#listLast(currentModality,'^')#' as Modality, vmth_person, Acct_Service.name as ServiceName,
p.Name as PatientName, Owner->Name "Client", p.Breed->Name "Patient_Breed_Name", 
        CASE WHEN p.Sex = 'Male' THEN 'M'
    	WHEN p.Sex = 'Female' THEN 'F'
    	WHEN p.Sex = 'Male Castrate' THEN 'M'
    	WHEN p.Sex = 'Female Spayed' THEN 'F'
    	WHEN p.Sex = 'Gelding' THEN 'M'
	 ELSE 'U' END As Patient_Sex,
    p.Species as Patient_Species, p.Date_Of_Birth as DOB,  
    Procedure_Requested as ProcedureCode, Acct_Procedure.Name as ProcedureName, PR."Date" as ScheduledStudyDateTime, 
    #listFirst(currentModality,'^')# as ModalityRowID,'#listGetAt(currentModality,2,'^')#' as AccessionPrefix
    FROM VMACS.#listFirst(currentModality,'^')# a
      INNER JOIN VMACS.#listFirst(currentModality,'^')#_Procedure_Requested PR ON PR.#listFirst(currentModality,'^')# = a.IEN
      INNER JOIN VMACS.Acct_Procedure on Procedure_Requested = Acct_Procedure.IEN
      INNER JOIN VMACS.Acct_Doctor on Clinician = Acct_Doctor.IEN
      INNER JOIN VMACS.Acct_Service on Acct_Doctor.Service = Acct_Service.Old_Code
     INNER JOIN VMACS.Vmth_Patient p ON p.IEN = a.Patient
    where 1=1
    
    AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
    AND Procedure_Requested NOT IN (#preserveSingleQuotes(excludedProcedures)#)
    ORDER BY Modality, Request, ProcedureCode, ModalityRowID
  </cfquery>
    
  <cfset lastRequestProc = ''>

  <cfoutput query="qImg" group="PatientNbr">
    
    <!--- See if we already have this patient in our list. If not, add them to the list and to the patient query --->
    <cfif ListFind(patientList,qImg.PatientNbr) eq 0>
      <cfset patientList = listAppend(patientList,qImg.PatientNbr)>
      
      <cfset queryAddRow(q)>
      <cfset querySetCell(q, "Patientid", qImg.PatientNbr)>
      <cfset querySetCell(q, "IssuerOfPatientId", "VMACS")>
      <cfset querySetCell(q, "LastName", listFirst(qImg.Client,','))>
      <cfset querySetCell(q, "FirstName", qImg.PatientName)>
      <cfset querySetCell(q, "Species", qImg.Patient_Species)>
      <cfset querySetCell(q, "Breed", qImg.Patient_Breed_Name)>
      <cfset querySetCell(q, "MiddleName", qImg.Patient_Species)>
      <cfset querySetCell(q, "MaidenName", qImg.Patient_Breed_Name)>
      <cfset querySetCell(q, "Sex", qImg.Patient_Sex)>
      <cfif len(qImg.DOB) gt 0>
      <cfset querySetCell(q, "DateOfBirth", DateFormat(createODBCDate(qImg.DOB),'yyyymmdd'))>
      </cfif>
    </cfif>
        
    <cfoutput>
    
      <!--- These rows need to have unique fillerOrderNumbers and AccessionNumbers --->
      <cfif lastRequestProc NEQ qImg.ModalityRowID & '^' & qImg.ProcedureCode>
        <!--- Add a ServiceRequest Row --->
        <cfset queryAddRow(qServiceRequest)>
        <cfset querySetCell(qServiceRequest, "patientid", qImg.PatientNbr)>
        <cfset querySetCell(qServiceRequest, "IssuerOfPatientId", "VMACS")>
        <cfset querySetCell(qServiceRequest, "FillerOrderNumber", "#qImg.AccessionPrefix##qImg.Request##qImg.ProcedureCode#")>
        <cfset querySetCell(qServiceRequest, "IssuerOfFillerOrderNumber", "VMACS")>
        <cfset querySetCell(qServiceRequest, "OrderPriority", "R")>
        <cfset querySetCell(qServiceRequest, "OrderCreationDateTime", DateFormat(createODBCDate(qImg.Date_Of_Request),'yyyymmdd'))>
        <cfset querySetCell(qServiceRequest, "Modality", qImg.Modality)>
        <cfset querySetCell(qServiceRequest, "OrderingClinician", qImg.vmth_person)>
        <cfset querySetCell(qServiceRequest, "OrderingService", qImg.ServiceName)>

        <!--- Add a RequestedProcedure Row --->
        <cfset queryAddRow(qRequestedProcedure)>
        <cfset querySetCell(qRequestedProcedure, "patientid", qImg.PatientNbr)>
        <cfset querySetCell(qRequestedProcedure, "IssuerOfPatientId", "VMACS")>
        <cfset querySetCell(qRequestedProcedure, "FillerOrderNumber", "#qImg.AccessionPrefix##qImg.Request##qImg.ProcedureCode#")>
        <cfset querySetCell(qRequestedProcedure, "IssuerOfFillerOrderNumber", "VMACS")>
        <cfset querySetCell(qRequestedProcedure, "ProcedureCode", qImg.ProcedureCode)>
        <cfif len(qImg.ProcedureName)>
          <cfset querySetCell(qRequestedProcedure, "ProcedureDescription", qImg.ProcedureName)>
        </cfif>
        <cfset querySetCell(qRequestedProcedure, "AccessionNumber", "#qImg.AccessionPrefix##qImg.Request##qImg.ProcedureCode#")>
        <cfset querySetCell(qRequestedProcedure, "RequestedProcedureID", "#qImg.AccessionPrefix##qImg.Request##qImg.ProcedureCode#")>
        <cfset querySetCell(qRequestedProcedure, "ScheduledProcedureStepID", "#qImg.AccessionPrefix##qImg.Request##qImg.ProcedureCode#")>
        <cfset querySetCell(qRequestedProcedure, "StudyStatus", "CM")>
        <cfset querySetCell(qRequestedProcedure, "ScheduledStudyDateTime", DateFormat(createODBCDate(qImg.ScheduledStudyDateTime),'yyyymmdd'))>
        <cfset querySetCell(qRequestedProcedure, "Modality", qImg.Modality)>

        <cfset lastRequestProc = qImg.ModalityRowID & '^' & qImg.ProcedureCode>
      </cfif> 
    </cfoutput>
   
  </cfoutput>
                
<cfset lastRequestProc = ''>
           
</cfloop>  

<!--- Do some validation --->
<!--- No duplicate FillerOrderNumbers in ServiceRequest--->
<cfquery dbtype="query" name="qCheck">
  SELECT count(*), fillerOrderNumber
  FROM qServiceRequest
  GROUP BY fillerOrderNumber
  HAVING Count(*) > 1
</cfquery>
      
<cfif qCheck.recordcount gt 0><h1>DuplicateFillerOrderNumbers in ServiceRequest</h1><cfabort></cfif>
      
<!--- No duplicate FillerOrderNumbers--->
<cfquery dbtype="query" name="qCheck">
  SELECT count(*), fillerOrderNumber
  FROM qRequestedProcedure
  GROUP BY fillerOrderNumber
  HAVING Count(*) > 1
</cfquery>
      
<cfif qCheck.recordcount gt 0><h1>DuplicateFillerOrderNumbers in RequestedProcedure</h1><cfabort></cfif> 
      
<cfif ExportToPSV>
    <cfset ServiceRequestPSV = QueryToCSV(qServiceRequest,"PatientID,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,OrderPriority,OrderCreationDateTime",True,"|")>
      <cffile action="write" file="#expandPath("./serviceRequest.csv")#" output="#left(ServiceRequestPSV,len(ServiceRequestPSV)-2)#" nameconflict="overwrite" addnewline="false"> 
<cfelse>
      
  <cfscript>   
    //Create a new sheet for the ServiceRequest Data. 
    SpreadsheetCreateSheet (theSpreadsheet, "ServiceRequest"); 
    SpreadsheetSetActiveSheet (theSpreadsheet, "ServiceRequest"); 
    SpreadsheetAddRow(theSpreadsheet,"PatientId,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,OrderPriority,OrderCreationDateTime,Modality,OrderingClinician,OrderingService");
    SpreadsheetAddRows(theSpreadsheet,qServiceRequest);
  </cfscript> 
</cfif>
 - DONE<cfflush>
    
<!--- PATIENT --->
<cfif ExportToPSV>
  <cfset PatientPSV = QueryToCSV(q,"Patientid,IssuerOfPatientId,LastName,FirstName,MiddleName,MaidenName,Sex,DateOfBirth",True,"|")>
    <cffile action="write" file="#expandPath("./patients.csv")#" output="#left(PatientPSV,len(PatientPSV)-2)#" nameconflict="overwrite" addnewline="false">    
<cfelse>
  <cfscript>   
    //Create a new sheet for the Patient Data. 
    SpreadsheetCreateSheet (theSpreadsheet, "Patient"); 
    SpreadsheetSetActiveSheet (theSpreadsheet, "Patient"); 
    SpreadsheetAddRow(theSpreadsheet,"PatientId,IssuerOfPatientId,LastName,FirstName,MiddleName,MaidenName,Sex,DateOfBirth");
    SpreadsheetAddRows(theSpreadsheet,q);
  </cfscript>       
</cfif>
      
<!--- Attachment - for species and breed --->
<cfloop query="q">
  <cfset queryAddRow(qAttachment)>
  <cfset querySetCell(qAttachment, "patientid", q.PatientId)>
  <cfset querySetCell(qAttachment, "IssuerOfPatientId", "VMACS")>
  <cfset querySetCell(qAttachment, "Type", "TX")>
  <cfset querySetCell(qAttachment, "Code", "SPECIES")>
  <cfset querySetCell(qAttachment, "Content", q.Species)>  
    
  <cfset queryAddRow(qAttachment)>
  <cfset querySetCell(qAttachment, "patientid", q.PatientId)>
  <cfset querySetCell(qAttachment, "IssuerOfPatientId", "VMACS")>
  <cfset querySetCell(qAttachment, "Type", "TX")>
  <cfset querySetCell(qAttachment, "Code", "BREED")>
  <cfset querySetCell(qAttachment, "Content", q.Breed)>  
</cfloop>

<cfif ExportToPSV>
  <cfset AttachmentCSV = QueryToCSV(qAttachment,"Patientid,IssuerOfPatientId,Type,Code,Content",True,"|")>
    <cffile action="write" file="#expandPath("./attachments.csv")#" output="#left(AttachmentCSV,len(AttachmentCSV)-2)#" nameconflict="overwrite" addnewline="false">    
<cfelse>
  <cfscript>   
    //Create a new sheet for the Patient Data. 
    SpreadsheetCreateSheet (theSpreadsheet, "Attachments"); 
    SpreadsheetSetActiveSheet (theSpreadsheet, "Attachments"); 
    SpreadsheetAddRow(theSpreadsheet,"Patientid,IssuerOfPatientId,Type,Code,Content");
    SpreadsheetAddRows(theSpreadsheet,qAttachment);
  </cfscript>       
</cfif>

      
      
      
  
<!--- REQUESTEDPROCEDURE --->
  <br>Requested Procedure<cfflush>
 
<cfif ExportToPSV>
    <cfset RequestedProcedurePSV = QueryToCSV(qRequestedProcedure,"PatientID,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,ProcedureCode,ProcedureDescription,AccessionNumber,RequestedProcedureID,ScheduledProcedureStepID,StudyStatus,ScheduledStudyDateTime,Modality",True,"|")>
      <cffile action="write" file="#expandPath("./requestedProcedure.csv")#" output="#left(RequestedProcedurePSV,len(RequestedProcedurePSV)-2)#" nameconflict="overwrite" addnewline="false">   
<cfelse>

        
<cfscript>   
  //Create a new sheet for the REQUESTEDPROCEDURE Data. 
  SpreadsheetCreateSheet (theSpreadsheet, "RequestedProcedure"); 
  SpreadsheetSetActiveSheet (theSpreadsheet, "RequestedProcedure"); 
  SpreadsheetAddRow(theSpreadsheet,"PatientId,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,ProcedureCode,ProcedureDescription,AccessionNumber,RequestedProcedureID,ScheduledProcedureStepID,StudyStatus,ScheduledStudyDateTime,Modality");
  SpreadsheetAddRows(theSpreadsheet,qRequestedProcedure);
</cfscript> 
</cfif>

<!--- REPORTS --->
<cfset modalityList = 'Ultrasonography,Sa_Radiography,La_Radiography,CT,MRI,Equine_Ultrasound,Equine_MRI,Nuclear_Medicine'>

<cfset qReport = queryNew("PatientId,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,AccessionNumber,CreatedDateTime,ValueType,Type,Body,Status,AuthorId,IssuerOfAuthorId,ValidatorId,IssuerOfValidatorId,Modality","integer,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,integer,varchar,varchar,varchar,varchar")>

  REPORTS
  
  <cfset lastRptRecordCount = 0>
    
  <cfloop list="#modalityList#" index="currentModality">
    <br><cfoutput>#currentModality#</cfoutput><cfflush>

      <cfswitch expression="#currentModality#">
        <cfcase value="La_Radiography">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
          SELECT PatientNbr, #currentModality#.Name as PatientName, Request, Invoice, #currentModality#_Study_Def.name as ReportSection, 
            Significant_Findings, '' as Overflow, Date_Of_Request, 'LAR' as AccessionPrefix, 'DX' as Modality, Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq, report_status 
          FROM VMACS.#currentModality#
              INNER JOIN VMACS.#currentModality#_Report_Summary on #currentModality#.IEN = #currentModality#_Report_Summary.#currentModality#
              INNER JOIN VMACS.#currentModality#_Study_Def ON #currentModality#_Study_Def.IEN = #currentModality#_Report_Summary.IEN
              INNER JOIN VMACS.#currentModality#_Procedure_Requested LARAD_PR ON LARAD_PR.LA_Radiography = #currentModality#.IEN
          where 1=1
            AND PatientNbr is not null
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested NOT IN (#PreserveSingleQuotes(ExcludedProcedures)#)
          order by Modality, #currentModality#_Report_Summary.#currentModality#, procedure_requested, #currentModality#_Study_Def.IEN
          </cfquery>
        </cfcase>

        <cfcase value="Sa_Radiography">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
          SELECT PatientNbr, #currentModality#.Name as PatientName, Request, Invoice, #currentModality#_Study_Def.name as ReportSection, 
            Significant_Findings, '' as Overflow, Date_Of_Request, 'SAR' as AccessionPrefix, 'DX' as Modality, Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq, report_status
          FROM VMACS.#currentModality#
              INNER JOIN VMACS.#currentModality#_Report_Summary on #currentModality#.IEN = #currentModality#_Report_Summary.#currentModality#
              INNER JOIN VMACS.#currentModality#_Study_Def ON #currentModality#_Study_Def.IEN = #currentModality#_Report_Summary.IEN
              INNER JOIN VMACS.SA_Radiography_Procedure_Requested SARAD_PR ON SARAD_PR.SA_Radiography = #currentModality#.IEN
          where 1=1
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested NOT IN (#PreserveSingleQuotes(ExcludedProcedures)#)
          order by Modality, #currentModality#_Report_Summary.#currentModality#, procedure_requested, #currentModality#_Study_Def.IEN
          </cfquery>
        </cfcase>
        
        
        <cfcase value="Ultrasonography">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
          SELECT PatientNbr, Ultrasonography.Name as PatientName, Request, Invoice, 
              Ultrasound_Study_Def.name as  ReportSection, Significant_Findings, '' As Overflow, Date_Of_Request, 
              'SAU' as AccessionPrefix, 'US' as Modality, Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq, report_status 
          FROM VMACS.Ultrasonography 
              INNER JOIN VMACS.Ultrasonography_Report_Summary on Ultrasonography.IEN = Ultrasonography_Report_Summary.Ultrasonography 
              INNER JOIN VMACS.Ultrasound_Study_Def ON Ultrasound_Study_Def.IEN = Ultrasonography_Report_Summary.IEN
              INNER JOIN VMACS.Ultrasonography_Procedure_Requested US_PR ON US_PR.Ultrasonography = Ultrasonography.IEN
          where 1=1 
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested NOT IN (#PreserveSingleQuotes(ExcludedProcedures)#)
          order by Modality, Ultrasonography_Report_Summary.Ultrasonography, procedure_requested, Ultrasound_Study_Def.IEN 
          </cfquery>
        </cfcase>
        
        <cfcase value="Equine_Ultrasound">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
	            SELECT PatientNbr, Equine_Ultrasound.Name as PatientName, Request, Invoice, 
              Eq_Ultrasound_Study_Def.name as  ReportSection, Significant_Findings, '' As Overflow, Date_Of_Request, 
              'EQU' as AccessionPrefix, 'US' as Modality, Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq, report_status 
          FROM VMACS.Equine_Ultrasound 
              INNER JOIN VMACS.Equine_Ultrasound_Report_Summary on Equine_Ultrasound.IEN = Equine_Ultrasound_Report_Summary.Equine_Ultrasound 
              INNER JOIN VMACS.Eq_Ultrasound_Study_Def ON Eq_Ultrasound_Study_Def.IEN = Equine_Ultrasound_Report_Summary.IEN
              INNER JOIN VMACS.Equine_Ultrasound_Procedure_Requested EqUS_PR ON EqUS_PR.Equine_Ultrasound = Equine_Ultrasound.IEN
          where 1=1 
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested NOT IN (#PreserveSingleQuotes(ExcludedProcedures)#)
          order by Equine_Ultrasound_Report_Summary.Equine_Ultrasound, procedure_requested, Eq_Ultrasound_Study_Def.IEN 
          </cfquery>
        </cfcase>
        
        
        <cfcase value="Equine_MRI">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
	             SELECT PatientNbr, Equine_Magnetic_Resonance_Imaging.Name as PatientName, Request, Invoice, 
              Equine_Mri_Study_Def.name as  ReportSection, Report_Summary as Significant_Findings, '' As Overflow, Date_Of_Request, 
              'EQM' as AccessionPrefix, 'MR' as Modality, Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq, report_status 
          FROM VMACS.Equine_Magnetic_Resonance_Imaging 
              INNER JOIN VMACS.Equine_Magnetic_Resonance_Imaging_Report_Summary on Equine_Magnetic_Resonance_Imaging.IEN = Equine_Magnetic_Resonance_Imaging_Report_Summary.Equine_Magnetic_Resonance_Imaging 
              INNER JOIN VMACS.Equine_Mri_Study_Def ON Equine_Mri_Study_Def.IEN = Equine_Magnetic_Resonance_Imaging_Report_Summary.IEN
              INNER JOIN VMACS.Equine_Magnetic_Resonance_Imaging_Procedure_Requested EqMRI_PR ON EqMRI_PR.Equine_Magnetic_Resonance_Imaging = Equine_Magnetic_Resonance_Imaging.IEN
          where 1=1 
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested NOT IN (#PreserveSingleQuotes(ExcludedProcedures)#)
          order by Equine_Magnetic_Resonance_Imaging_Report_Summary.Equine_Magnetic_Resonance_Imaging, procedure_requested, Equine_Mri_Study_Def.IEN 
          </cfquery>
        </cfcase>
        
        
        <cfcase value="CT">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
          SELECT PatientNbr, Ct_Scanning.Name as PatientName, Request, Invoice, 
            Ct_Scanning_Study_Def.name as ReportSection, Report_Summary as Significant_Findings, Report_Summary_Overflow as Overflow, 
            Date_Of_Request, 'CT' as AccessionPrefix, 'CT' as Modality, Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq, report_status 
          FROM VMACS.Ct_Scanning 
            INNER JOIN VMACS.Ct_Scanning_Report_Summary on Ct_Scanning.IEN = Ct_Scanning_Report_Summary.Ct_Scanning 
            INNER JOIN VMACS.Ct_Scanning_Study_Def ON Ct_Scanning_Study_Def.IEN = Ct_Scanning_Report_Summary.IEN
            INNER JOIN VMACS.Ct_Scanning_Procedure_Requested CT_PR ON CT_PR.CT_Scanning = CT_Scanning.IEN
          where 1=1 
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested NOT IN (#PreserveSingleQuotes(ExcludedProcedures)#)
          ORDER BY  Ct_Scanning_Report_Summary.Ct_Scanning, procedure_requested, Ct_Scanning_Study_Def.IEN 
          </cfquery>
        </cfcase>
        
              
        <cfcase value="MRI">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
            SELECT PatientNbr, Magnetic_Resonance_Imaging.Name as PatientName, Request, Invoice, 
              MRI_Study_Def.name as ReportSection, Report_Summary as Significant_Findings, Incidental_Findings as Overflow, 
              Date_Of_Request, 'MRI' as AccessionPrefix, 'MR' as Modality, Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq, report_status 
            FROM VMACS.Magnetic_Resonance_Imaging INNER JOIN VMACS.Magnetic_Resonance_Imaging_Report_Summary on Magnetic_Resonance_Imaging.IEN = Magnetic_Resonance_Imaging_Report_Summary.Magnetic_Resonance_Imaging 
            INNER JOIN VMACS.MRI_Study_Def ON MRI_Study_Def.IEN = Magnetic_Resonance_Imaging_Report_Summary.IEN 
            INNER JOIN VMACS.Magnetic_Resonance_Imaging_Procedure_Requested MRI_PR ON MRI_PR.Magnetic_Resonance_Imaging = Magnetic_Resonance_Imaging.IEN
            where 1=1 
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested NOT IN (#PreserveSingleQuotes(ExcludedProcedures)#)
            ORDER BY  Magnetic_Resonance_Imaging_Report_Summary.Magnetic_Resonance_Imaging, procedure_requested, MRI_Study_Def.IEN 
          </cfquery>
        </cfcase>
        
          <cfcase value="Nuclear_Medicine">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
            SELECT PatientNbr, Nuclear_Medicine.Name as PatientName, Request, Invoice, 
              Nuclear_Medicine_Study_Def.name as ReportSection, Significant_Findings, Incidental_Findings as Overflow, 
              Date_Of_Request, 'NM' as AccessionPrefix, 'NM' as Modality, Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq, report_status 
            FROM VMACS.Nuclear_Medicine INNER JOIN VMACS.Nuclear_Medicine_Report_Summary on Nuclear_Medicine.IEN = Nuclear_Medicine_Report_Summary.Nuclear_Medicine 
            INNER JOIN VMACS.Nuclear_Medicine_Study_Def ON Nuclear_Medicine_Study_Def.IEN = Nuclear_Medicine_Report_Summary.IEN 
            INNER JOIN VMACS.Nuclear_Medicine_Procedure_Requested NM_PR ON NM_PR.Nuclear_Medicine = Nuclear_Medicine.IEN
            where 1=1 
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested NOT IN (#PreserveSingleQuotes(ExcludedProcedures)#)
            ORDER BY  Nuclear_Medicine_Report_Summary.Nuclear_Medicine, procedure_requested, Nuclear_Medicine_Study_Def.IEN 
          </cfquery>
        </cfcase>        
    
    </cfswitch>
    
<cfoutput>Total #qRpt.recordcount# rows<br></cfoutput>

<cfoutput query="qRpt" group="procReq">
  <cfset queryAddRow(qReport)>
  <cfset querySetCell(qReport, "patientid", qRpt.PatientNbr)>
  <cfset querySetCell(qReport, "IssuerOfPatientId", "VMACS")>
  <cfset querySetCell(qReport, "FillerOrderNumber", "#qRpt.AccessionPrefix##qRpt.Request##qRpt.Procedure_Requested#")>
  <cfset querySetCell(qReport, "IssuerOfFillerOrderNumber", "VMACS")>
  <cfset querySetCell(qReport, "AccessionNumber", "#qRpt.AccessionPrefix##qRpt.Request##qRpt.Procedure_Requested#")>
  <cfset querySetCell(qReport, "CreatedDateTime", DateFormat(createODBCDate(qRpt.Date_Of_Request),'yyyymmdd'))>
  <cfset querySetCell(qReport, "ValueType", "FT")>
  <cfset querySetCell(qReport, "Type", "GDT")>
  <cfif len(qRpt.Report_Status)>
    <cfswitch expression="#qRpt.Report_Status#">
      <cfcase value="DRAFT,PRELIMINARY"> 
        <cfset querySetCell(qReport, "Status", "P")>
      </cfcase>
      <cfcase value="FINAL">
        <cfset querySetCell(qReport, "Status", "F")>
      </cfcase>
      <cfdefaultcase>
        <h1>Error with report status (#qRpt.Report_Status#)- #qRpt.Modality##qRpt.Request##qRpt.Procedure_Requested#</h1> 
        <cfabort>
      </cfdefaultcase>
    </cfswitch>
  <cfelse> <!--- Some draft status reports have no value in status --->
    <cfset querySetCell(qReport, "Status", "P")>
  </cfif>
  <cfset querySetCell(qReport, "IssuerOfAuthorId", "SYSTEM")>
  <cfset querySetCell(qReport, "IssuerOfValidatorId", "SYSTEM")>
  <cfset querySetCell(qReport, "Modality", qRpt.Modality)>

  <cfset Body = ''>
  <cfoutput>  
    <cfset theSigFindings = replace(trim(qRpt.Significant_Findings),'#chr(13)##chr(10)#','\.br\\.br\', 'all')>
    <cfset theOverflow = replace(trim(qRpt.Overflow),'#chr(13)##chr(10)#','\.br\\.br\', 'all')>
    <cfset Body = Body & '\.br\\.br\' & qRpt.ReportSection & '\.br\\.br\' & theSigFindings & theOverflow & '\.br\\.br\'>
    <cfif qRpt.currentRow%100 eq 0>#NumberFormat(qRpt.currentRow/qRpt.recordcount*100,'9.9')#% * <cfflush></cfif>
  </cfoutput>
  <cfset querySetCell(qReport, "Body", trim(body))>

    
  <cfset AuthorID = 0>    
  <cfset ValidatorID = 0>

  <!---<cfif find("K. Brust DVM.",body)>
    <cfset AuthorID = 6807>
  <cfelseif find("Amy Norvall, DVM",body)>
    <cfset AuthorID = 9257>
  <cfelseif find("Britton Nixon DVM",body)>
    <cfset AuthorID = 9257>
      
  <cfelse>
    <cfset AuthorID = 0>
  </cfif>
    
  <cfif find("..REP.",body)>
    <cfset ValidatorID = 1368>
  <cfelseif find("..EGJ",body)>
    <cfset ValidatorID = 1757>
  <cfelseif find("..BV",body)>
    <cfset ValidatorID = 3758>
  <cfelseif find("..MBW",body)>
    <cfset ValidatorID = 876>
  <cfelseif find("..ARZ",body)>
    <cfset ValidatorID = 4821>
  <cfelse>
    <cfset ValidatorID = 0>
  </cfif>  
--->  
    
  <cfset querySetCell(qReport, "AuthorId", AuthorID)>
  <cfset querySetCell(qReport, "ValidatorID", ValidatorID)>

</cfoutput>
<cfoutput><br>Total #qReport.recordcount-lastRptRecordCount# records<br></cfoutput>
<cfset lastRptRecordCount = qReport.recordcount>
      

</cfloop> 
      
<!--- More validation --->
<!--- does every accessionID in Reports have an entry in RequestedProcedures? --->
<cfset ReportAccessionIDs = ValueList(qReport.AccessionNumber)>
<cfset RequestedProcedureAccessionIDs = ValueList(qRequestedProcedure.AccessionNumber)>
  
<cfset error = 0>
<cfloop list="#ReportAccessionIds#" index="currentReport">
  <cfif listFind(RequestedProcedureAccessionIDs,currentReport) eq 0>
    <h1>MISSING Associated RequestedProcedure - <cfoutput>#currentReport#</cfoutput></h1>
    <cfset error = 1>
  </cfif>
</cfloop>

<cfif error eq 1><cfabort></cfif>
  
<!--- Does every entry in RequestedProcedures have an entry in ServiceRequest? --->
<cfset ServiceRequestFillerOrderNumbers = ValueList(qServiceRequest.FillerOrderNumber)>
  
<cfset error = 0>
<cfloop list="#RequestedProcedureAccessionIDs#" index="currentProcedure">
  <cfif listFind(ServiceRequestFillerOrderNumbers,currentProcedure) eq 0>
    <h1>MISSING Associated ServiceRequest - <cfoutput>#currentProcedure#</cfoutput></h1>
    <cfset error = 1>
  </cfif>
</cfloop>

<cfif error eq 1><cfabort></cfif>
  
      
<cfif ExportToPSV>
    <cfset ReportPSV = QueryToCSV(qReport,"PatientID,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,AccessionNumber,CreatedDateTime,ValueType,Type,Body,Status,AuthorId,IssuerOfAuthorId,ValidatorId,IssuerOfValidatorId",True,"|")>
      <cffile action="write" file="#expandPath("./report.csv")#" output="#left(ReportPSV,len(ReportPSV)-2)#" nameconflict="overwrite" addnewline="false">   
      <cfabort>
<cfelse>

      <cfscript>   
  //Create a new sheet for the REPORT Data. 
  SpreadsheetCreateSheet (theSpreadsheet, "Report"); 
  SpreadsheetSetActiveSheet (theSpreadsheet, "Report"); 
  SpreadsheetAddRow(theSpreadsheet,"PatientId,IssuerOfPatientId,FillerOrderNumber,IssuerOfFillerOrderNumber,AccessionNumber,CreatedDateTime,ValueType,Type,Body,Status,AuthorId,IssuerOfAuthorId,ValidatorId,IssuerOfValidatorId,Modality");
  SpreadsheetAddRows(theSpreadsheet,qReport);
</cfscript>       
  </cfif>
 
<!--- Write the spreadsheet to a file, replacing any existing file. ---> 
<cfspreadsheet action="write" filename="#theFile#" name="theSpreadsheet" overwrite=true>

  <h1>DONE</h1>