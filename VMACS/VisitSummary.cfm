<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Visit Summary Mockup</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="well">  
    <div class="row">
      <div class="col-md-12"><h1>Visit 4213AQ <button type="button" class="btn btn-sm btn-primary">Info &amp; Tasks</button> 
    	<button type="button" class="btn btn-sm btn-primary">Sections</button>
        <button type="button" class="btn btn-sm btn-primary">Print</button>
        <button type="button" class="btn btn-sm btn-primary">&lt;</button>
        <button type="button" class="btn btn-sm btn-primary">&gt;</button></h1></div></div>

<div class="row">
  <div class="col-md-6"><small class="form-text text-muted">Patient</small><br>
        <a href="#">49-56-87</a> Tubby [29Sep13-FE-MC-DSH] <span class="label label-danger">BITES</span> <span class="label label-danger">DNR</span><br><span class="label label-warning">Warning text about the patient (Patient Caution)</span> <span class="label label-warning">Patient Allergies</span> 
</div>
  <div class="col-md-6"><p>
        <small class="form-text text-muted">Client</small><br>
        <a href="#">C38562</a> Carlisle, Marie MD &amp; Brian
      </p></div>
</div>
<div class="row">
  <div class="col-md-3">
    <small class="form-text text-muted">Weight</small><br>
    <a href="#">45.6 kg</a>
  </div>
  <div class="col-md-3">
    <small class="form-text text-muted">Service</small><br>
    <a href="#">Small Animal Medicine</a>
  </div>
  <div class="col-md-3">
            <small class="form-text text-muted">Referring Providers</small><br>
    Primary - <a href="#">Animal Medical Center</a>
</div>
<div class="col-md-3">
   <small class="form-text text-muted">Estimate</small><br>
   <a href="#">$1265.40</a>
</div>

</div>

<div class="row">
  <div class="col-md-3"><small class="form-text text-muted">Location</small><br>
        <a href="#">415</a></div>
  <div class="col-md-3"><small class="form-text text-muted">Clinician</small><br>
        <a href="#">Johnson, Lynelle</a> 530-219-1234 <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></div>
  <div class="col-md-3"><small></small><br>Referral - <a href="#">Companion Veterinary Clinic</a></div>
  <div class="col-md-3"><small class="form-text text-muted">Charges</small><br>
        $1000.20
  </div>
</div>

<div class="row">
  <div class="col-md-3"><small class="form-text text-muted">Admit</small><br>
    S Out - 10Oct17 @ 9:32 AM</div>
  <div class="col-md-3"><small class="form-text text-muted">Faculty</small><br>
        <a href="#">Wilson, David</a> 530-219-1234 <span class="glyphicon glyphicon-info-sign" aria-hidden="true" title="Additional Details"></span></div>
  <div class="col-md-3"><small></small><br>Specialist - <a href="#">Davis Orthopedic Clinic</a></div>
  <div class="col-md-3"><small class="form-text text-muted">Payments</small><br>
        $800.20
  </div>
</div>

<div class="row">
  <div class="col-md-3"><small class="form-text text-muted">Discharge</small><br>
    [Alive] - 10Oct17 - Closed</div>
  <div class="col-md-3"><small class="form-text text-muted">Student</small><br>
        <a href="#">Testcase, Student</a>  530-219-1234 <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></div>
  <div class="col-md-3"></div>
  <div class="col-md-3"><small class="form-text text-muted">TA</small><br>
        $1.00
  </div>
</div>

<div class="row">
  <div class="col-md-6"><small class="form-text text-muted">Comment</small><br>
    This is the big comment for this visit</div>
  <div class="col-md-3"><small class="form-text text-muted">Communication</small><br>
    <a href="#">4 Client / 2 RDVM / 3 Internal</a>
  </div>
  <div class="col-md-3"><small class="form-text text-muted">Balance</small><br>
        $200.00
  </div>
</div>
</div>


<div class="well">
<h3><a name=PC>Presenting Complaints</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="PC_content"><ul>
<li><span id="PC1">Painful Right Shoulder</span>
</ul></div>

<h3><a name=PH>Pertinent History</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="PH_content"><ul>
<li><span id="PH1">Murphy presented to the VMTH Neurology Service for evaluation of a right forelimb lameness.</span>
<li><span id="PH2">The owner reports that Murphy was acutely lame on the right front lame on 4/5/04 after climbing out of a ditch during a walk. He has progressively gotten worse, and now appears to be in considerable pain. He holds the right front paw off the ground at rest and will sleep with it held out to the side. He has pain on rising and laying down, but will jump into the car. The owner is worried about an injury to the shoulder and brachial plexus region.</span>
<li><span id="PH3">Murphy has been panting and pacing for the last few nights and his appetite has decreased for one week. He is still drinking water normally.</span>
<li><span id="PH4">He was treated with a fentanyl patch yesterday (100ug) at the rDVM. Referral radiographs showed severe DJD in both elbows.</span>
<li><span id="PH5">Murphy has a history of osteitis in both thoracic limbs when he was six months old. He has had an intermittent left forelimb lameness since then. He has been treated with Adequan 1.5cc every 2-3 weeks starting two months ago for his left forelimb. This last week he received two doses, the last was four days ago, as the owner noticed limping in the left thoracic limb.</span>
<li><span id="PH6">Current on vaccines, no T/T/T/T. No C/S/V/D.</span>
<li><span id="PH7">Murphy is an indoor/outdoor dog and has access to a fenced yard. he eats Solid Gold dog food with a little meat mixed in.</span>
</ul></div>

<h3><a name=PE>Physical Examination</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="PE_content"><ul>
<li><span id="PE1">Gen: Alert, responsive, hydrated</span>
<li><span id="PE2">Integ: soft, full haircoat,</span>
<li><span id="PE3">EENT: anterior chamber, sclera clear, conjunctiva mildly injected,no ocular discharge pupils equal and responsive direct and consensual, no nasal discharge, ears wnl.</span>
<li><span id="PE4">MS: BCS 6/9, ambulatory, right thoracic limb lameness</span>
<li><span id="PE5">CV: HR 120, no murmurs or arrhythmias ausculted, femoral pulses strong and synchronous, mm pink, CRT&lt; 2</span>
<li><span id="PE6">Resp: RR pant, clear lungs sounds in all quadrants</span>
<li><span id="PE7">GI: abdomen soft, nonpainful, no masses or organomegaly palpated, rectal wnl - no apparent pain on palpation, no masses palpable.</span>
<li><span id="PE8">GU: bladder, kidneys not palpated</span>
<li><span id="PE9">NS:</span>
<li><span id="PE10">Mentation: Alert and responsive</span>
<li><span id="PE11">Gait/posture: Ambulatory, right thoracic limb lameness, kyphotic and holding neck down.</span>
<li><span id="PE12">Cranial nerves: wnl</span>
<li><span id="PE13">Postural reactions: CP decreased in right thoracic limb, all others wnl</span>
<li><span id="PE14">Segmental reflexes: Myotatic reflexes wnl, withdrawals wnl, tail tone and anal tone wnl. Panniculus wnl</span>
<li><span id="PE15">Spinal palpation: Apparent cervical pain</span>
<li><span id="PE16">LN: mandibular, superficial cervical, popliteal lymph nodes wnl</span>
</ul></div>

<h3><a name=P>Problems</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="P_content"><ol>
<li><span id="P1">C1-T2 myelopathy renamed to C5-6 myelopathy secondary to extradural mass on 4/16/04</span>
<li><span id="P2">Multifocal spinal pain - inactivate</span>
<li><span id="P3">Urinary Tract Infection (Staph)</span>
</ol></div>

<h3><a name=MSP>Medical/Surgical Procedures</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="MSP_content"><ol>
<li><span id="MSP1">4/14/04 CBC: WBW=16580, Neutrophils=12137, PP=8.3, Hct=54.7%.</span>
<li><span id="MSP2">4/14/04 Neuro panel= anion gap=26 (12-25), Na=159 (145-154), Cl=121 (105-116), Ca=12.2 (9.9-11.4), BUN=35 (8-31), TP=8.0 (5.4-7.4), Globulin 4.7 (2.3-4.4), ALT=88 (19-67), Alk Phos=894 (15-127), Choles= 383 (135-345), CK=546 (46-320)</span>
<li><span id="MSP3">4/14/04 Abdominal US: The kidneys have an irregular outer contour and poor internal architectural definition. The renal cortices are mildly echogenic. There is mild fluid dilatation of the renal pelves bilaterally with mild flattening of the renal crests. There is a small amount of sediment within the urinary bladder. The other abdominal structures are unremarkable. The axillary regions were evaluated and no mass lesions are identified. Impression: The renal changes suggest chronic pyelonephritis.</span>
<li><span id="MSP4">4/14/04 Urinalysis: Sp grav=1.020, 3+ hemoprotein, 2+ protein, 2-5WBC/hpf, &gt;100 RBC/hpf. No bacteria seen.</span>
<li><span id="MSP5">4/15/04 Urine culture: Staphylococcus sp. susceptible to baytril, tetracycline, clavamox, TMS, Ampicillin and cephalexin.</span>
<li><span id="MSP6">4/15/04 Ionized Ca=1.33</span>
<li><span id="MSP7">4/15/04 Spinal rads: unremarkable</span>
<li><span id="MSP8">4/15/04 Myelogram: A myelogram was performed with good subarachnoid filling. There is encroachment of the facets on the contrast columns dorsally at C4-5. There is also dorsal deviation of the ventral contrast column at C6-7. Dorsoventral oblique views review an extradural cord compressive lesion at C5-6. Extradural cord compressive lesion at C5-6 most likely extruded disc material. Cord impinging lesions dorsally at C4-5 and ventrally at C6-7.</span>
<li><span id="MSP9">4/15/04 CT: Showed compressive lesion on the right dorsal C5-6.</span>
<li><span id="MSP10">4/15/04 Dorsal laminectomy at C5-6. mass removed from right dorsal spinal cord. Recovered in ICU.</span>
</ol></div>

<h3><a name=MXP>Medical/Surgical Procedures (Billing)</a> </h3>

<table class="table table-bordered table-responsive table-striped table-condensed"><thead><tr>
<th>Service</th><th>Request</th><th>&nbsp;</th><th>Date</th><th>Procedure</th><th>Image</th>
</tr><tbody>
<tr><td>Neurology/Neurosurgery</td><td>&nbsp;</td><td></td><td>14Apr04</td><td>1150 - Neurology Exam</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Anesthesia</td><td><a href="/san/A042121">A042121</a></td><td></td><td>15Apr04</td><td>5225 - Anes Recovery</td><td>&nbsp;</td></tr>
<tr><td>Central Services</td><td>&nbsp;</td><td></td><td>16Apr04</td><td>0984 - Misc Supplies</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td>&nbsp;</td><td></td><td>16Apr04</td><td>0984 - Misc Supplies</td><td>&nbsp;</td></tr>
<tr><td>Neurology/Neurosurgery</td><td><a href="/neu/N040207">N040207</a></td><td>Surger</td><td>16Apr04</td><td>2507 - Laminectomy</td><td align="center"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></td></tr>
<tr class="row_even"><td>SA Anesthesia</td><td><a href="/san/A042137">A042137</a></td><td></td><td>16Apr04</td><td>5004 - Anesthesia, Level 4</td><td>&nbsp;</td></tr>
</table>

<h3><a name=DXP>Diagnostic Procedures</a> <span class="glyphicon glyphicon-th" aria-hidden="true"></span></h3>

<table class="table table-bordered table-responsive table-striped table-condensed"><thead><tr><th>Service</th><th>Request</th><th>Status</th><th>Date</th><th>Procedure</th><th>Image</th></tr><tbody>
<tr><td>Hematology/Cytology</td><td><a href="/hem/AQ4781">AQ4781</a></td><td>Final</td><td>14Apr04</td><td>7006 - Complete blood count</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>Clinical Chemistry</td><td><a href="/chm/BB5453">BB5453</a></td><td>Final</td><td>14Apr04</td><td>7754 - Neuromuscular Panel</td><td>&nbsp;</td></tr>
<tr><td>SA Ultrasound</td><td><a href="/usn/43821">43821</a></td><td>Final</td><td>14Apr04</td><td>6900 - Sa Ultrasound Scan</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Ultrasound</td><td><a href="/usn/43821">43821</a></td><td>Final</td><td>14Apr04</td><td>6989 - Misc Ultrasound</td><td>&nbsp;</td></tr>
<tr><td>Clinical Chemistry</td><td><a href="/chm/BB5501">BB5501</a></td><td>Final</td><td>14Apr04</td><td>7452 - Urinalysis</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>Bacteriology/Mycology</td><td><a href="/bac/0402837">0402837</a></td><td>Final</td><td>14Apr04</td><td>8031 - Urine Aerobic C&s</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123000">123000</a></td><td>Prelim</td><td>15Apr04</td><td>7950 - Blood Gas (ICU)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123000">123000</a></td><td>Prelim</td><td>15Apr04</td><td>7951 - Electrolytes (ICU)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123000">123000</a></td><td>Prelim</td><td>15Apr04</td><td>7962 - Glucose (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123000">123000</a></td><td>Prelim</td><td>15Apr04</td><td>7963 - Lactate (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr><td>Hematology/Cytology</td><td><a href="/hem/AQ4852">AQ4852</a></td><td>Final</td><td>15Apr04</td><td>7127 - Fluid - spinal</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Radiography</td><td><a href="/sar/111513">111513</a></td><td>Final</td><td>15Apr04</td><td>6041 - Vertebral Col Study</td><td>&nbsp;</td></tr>
<tr><td>SA Radiography</td><td><a href="/sar/111513">111513</a></td><td>Final</td><td>15Apr04</td><td>6219 - Myelogram</td><td align="center"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></td></tr>
<tr class="row_even"><td>SA Radiography</td><td><a href="/sar/111513">111513</a></td><td>Final</td><td>15Apr04</td><td>6071 - X-Ray Bil Elbows</td><td>&nbsp;</td></tr>
<tr><td>CT Scanning</td><td><a href="/cts/6435">6435</a></td><td>Final</td><td>15Apr04</td><td>6600 - CT Scan</td><td align="center"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></td></tr>
<tr class="row_even"><td>Hematology/Cytology</td><td><a href="/hem/AQ4958">AQ4958</a></td><td>Final</td><td>16Apr04</td><td>7003 - Blood cross-match</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123034">123034</a></td><td>Prelim</td><td>16Apr04</td><td>7950 - Blood Gas (ICU)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123034">123034</a></td><td>Prelim</td><td>16Apr04</td><td>7951 - Electrolytes (ICU)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123034">123034</a></td><td>Prelim</td><td>16Apr04</td><td>7962 - Glucose (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123034">123034</a></td><td>Prelim</td><td>16Apr04</td><td>7963 - Lactate (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123038">123038</a></td><td>Prelim</td><td>16Apr04</td><td>7950 - Blood Gas (ICU)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123038">123038</a></td><td>Prelim</td><td>16Apr04</td><td>7951 - Electrolytes (ICU)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123038">123038</a></td><td>Prelim</td><td>16Apr04</td><td>7962 - Glucose (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123038">123038</a></td><td>Prelim</td><td>16Apr04</td><td>7963 - Lactate (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123049">123049</a></td><td>Prelim</td><td>16Apr04</td><td>7950 - Blood Gas (ICU)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123049">123049</a></td><td>Prelim</td><td>16Apr04</td><td>7951 - Electrolytes (ICU)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123049">123049</a></td><td>Prelim</td><td>16Apr04</td><td>7962 - Glucose (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123049">123049</a></td><td>Prelim</td><td>16Apr04</td><td>7963 - Lactate (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123052">123052</a></td><td>Prelim</td><td>16Apr04</td><td>7950 - Blood Gas (ICU)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123052">123052</a></td><td>Prelim</td><td>16Apr04</td><td>7951 - Electrolytes (ICU)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123052">123052</a></td><td>Prelim</td><td>16Apr04</td><td>7962 - Glucose (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123052">123052</a></td><td>Prelim</td><td>16Apr04</td><td>7963 - Lactate (Saicu Abl)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123059">123059</a></td><td>Final</td><td>16Apr04</td><td>7960 - ICU Panel 1</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123079">123079</a></td><td>Prelim</td><td>17Apr04</td><td>7950 - Blood Gas (ICU)</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123079">123079</a></td><td>Prelim</td><td>17Apr04</td><td>7951 - Electrolytes (ICU)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123093">123093</a></td><td>Prelim</td><td>17Apr04</td><td>7960 - ICU Panel 1</td><td>&nbsp;</td></tr>
<tr><td>SA Emergency Critical Care</td><td><a href="/sic/123113">123113</a></td><td>Prelim</td><td>18Apr04</td><td>7950 - Blood Gas (ICU)</td><td>&nbsp;</td></tr>
<tr class="row_even"><td>SA Emergency Critical Care</td><td><a href="/sic/123113">123113</a></td><td>Prelim</td><td>18Apr04</td><td>7951 - Electrolytes (ICU)</td><td>&nbsp;</td></tr>
<tr><td>Histopathology</td><td><a href="/pth/04B1046">04B1046</a></td><td>Final</td><td>18Apr04</td><td>9012 - Biopsy Interpret</td><td align="center"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></td></tr>
<tr class="row_even"><td>Histopathology</td><td><a href="/pth/04B1046">04B1046</a></td><td>Final</td><td>18Apr04</td><td>9322 - Dx Immuno Stains</td><td>&nbsp;</td></tr>
<tr><td>Histopathology</td><td><a href="/pth/04B1046">04B1046</a></td><td>Final</td><td>18Apr04</td><td>9015 - Dx Special Stains</td><td>&nbsp;</td></tr>
</table>

<h3><a name="DOC">Attached Docs</a>&nbsp;&nbsp;<span class="glyphicon glyphicon-plus" aria-hidden="true"></span></h3>

<h3><a name="IMG">Attached Images</a>&nbsp;&nbsp;<span class="glyphicon glyphicon-plus" aria-hidden="true"></span></h3>

<h3><a name=PTH>Pathologic Diagnosis</a> - <a href="/pth/04B1046">04B1046</a> - FINAL</h3>
<ol>
<li>C5-C6 EXTRADURAL MASS: LEIOMYOSARCOMA (SEE COMMENT)
</ol>

<h3><a name=CD>Clinical Diagnoses</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="CD_content"><ol>
<li><span id="CD1">C5-6 myelopathy secondary to extradural leiomyosarcoma</span>
<li><span id="CD2">Pyelonephritis (Staph UTI)</span>
</ol></div>

<h3><a name=PPN>Plans and Progress Notes</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="PPN_content"><ol>
<li><span id="PPN1">4/14/04</span>
<li><span id="PPN2">Problem 1 (C1-T2 myelopathy): S/O: See history and physical exam. T=101.7, P=130, R=pant. Given hydromorphone 2mg at 10pm. CBC showed WBW=16580, Neutrophils=12137, PP=8.3, Hct=54.7%. Neuro panel showed anion gap=26 (12-25), Na=159 (145-154), Cl=121 (105-116), Ca=12.2 (9.9-11.4), BUN=35 (8-31), TP=8.0 (5.4-7.4), Globulin 4.7 (2.3-4.4), ALT=88 (19-67), Alk Phos=894 (15-127), Choles= 383 (135-345), CK=546 (46-320). U/S of the brachial plexus area was unremarkable. A: Stable. Lesion localized to C1-T2 myelopathy. May have a T3-L3 myelopathy as well based on spinal pain and CP deficits in LPL that is most likely a chronic issue that is not causing the current clinical signs. Murphy is very painful, which may indicate nerve root involvement. CBC showed mild leukocytosis which may be due to stress or underlying UTI. Neuro panel showed hypercalcemia, increased ALP and increased globulins which may indicate bone lysis due to neoplasia. P: Myelogram tomorrow to localize lesion. Continue to monitor and treat for pain. Hydromorphone q6hr PRN. Start LRS @120ml/hr.</span>
<li><span id="PPN3">Problem 2 (Multifocal spinal pain): S/O: See history and physical exam. A: Murphy has both lumbar and cervical pain, which may indicate that she has a multifocal myelopathy. Differentials for multifocal spinal pain include discospondylitis (related to her UTI), myelitis, meningitis, polyarthritis, histiocytosis, neoplasia. P: Myelogram tomorrow. Continue to monitor and treat for pain. Hydromorphone q6hr PRN.</span>
<li><span id="PPN4">Problem 3 (UTI): S/O: Urinalysis showed Sp grav=1.020, 3+ hemoprotein, 2+ protein, 2-5WBC/hpf, &gt;100 RBC/hpf. No bacteria seen. Urine culture pending. Abdominal U/S showed that the renal cortices are mildly echogenic with mild fluid dilatation of the renal pelves. There is a small amount of sediment within the urinary bladder. CBC showed WBW=16580, Neutrophils=12137. A: UTI based on inflammatory cells in urine from cysto. CBC showed mild leukocytosis which may be due to stress or underlying UTI. Increased BUN, Na and Cl may be due to pre-renal azotemia due to dehydration, as owner has not offered water for 24 hours. Mildly increased BUN is consistent with U/S findings of chronic pyelonephritis and WBC in urine which may indicate a concurrent UTI that led to an ascending infection. However, BUN increase may be due to dehydration as well. No renal failure at this time. P: LRS @120ml/hr. Antibiotics pending urine culture.</span>
<li><span id="PPN5">4/15/04</span>
<li><span id="PPN6">Problem 1 (C1-T2 myelopathy): S/O: T=100.4, P=116, R=pant. Given hydromorphone 2mg at 4am due to agitation and discomfort. Myelogram showed encroachment of the facets on the contrast columns dorsally at C4-5. There is also dorsal deviation of the ventral contrast column at C6-7. Dorsoventral oblique views review an extradural cord compressive lesion at C5-6. Extradural cord compressive lesion at C5-6 most likely extruded disc material. Cord impinging lesions dorsally at C4-5 and ventrally at C6-7. CT showed a compressive lesion on the right ventral side of C5 over the disk space. A: Differentials for this extradural compressive mass include neoplasia (meningioma, lymphoma, histiocytic sarcoma), disk protrusion or, less likely, infection. P: Surgery tomorrow for a dorsal laminectomy. Cross-match before surgery. Continue to monitor pain and treat as above. Continue fluid therapy.</span>
<li><span id="PPN7">Problem 2 (Multifocal spinal pain): S/O: T=100.4, P=116, R=pant. Given hydromorphone 2mg at 4am due to agitation and discomfort. Myelogram and spinal rads did not show anything to suggest a lesion in the lumbar region. Lumbar pain may be due to a chronic problem. A: Stable. P: Continue to monitor for pain and treat as above. Inactivate problem.</span>
<li><span id="PPN8">Problem 3 (UTI): S/O: Urine culture grew Staphylococcus. Given ampicillin 22mg/kg for UTI. A: Stable. Urine output is good. P: Continue with fluid therapy and ampicillin.</span>
<li><span id="PPN9">4/16/04</span>
<li><span id="PPN10">Problem 1 (C5-6 myelopathy): S/O: T=100.1, P=90, R=pant. Dorsal laminectomy in AM. Mass at C5-6 removed and spinal cord decompressed. Nerve root involvement may have caused a lot of pain, so she was recovered in ICU. Given 0.04mg/kg oxymorphone at extubation (6pm) and at 8:30pm. Treated with Ketamine CRI at 0.4 mg/kg/hr, morphine 0.3mg/kg/hr and Acepromazine 0.02 mg/mg q2hr. LRS @120ml/kg/hr. She has been crying frequently and is very sensitive to being touched. Edema at incision site treated with cold packs. Urinary catheter was placed post-op. A: Mass removal went well, but increased pain and agitation post-operatively may be due to nerve root involvement. P: Continue to monitor for pain and agitation and treat with Ketamine CRI at 0.4 mg/kg/hr, morphine 0.3mg/kg/hr and Acepromazine 0.02 mg/mg q2hr. Keep quiet and comfortable. Continue LRS @120ml/hr. Monitor UOP.</span>
<li><span id="PPN11">Problem 3 (UTI): T=100.1. Given ampicillin 22mg/kg. Urinary catheter placed post-op. A: Stable. P: Monitor urine output. Urinary catheter care. Continue fluid therapy and ampicillin as above.</span>
<li><span id="PPN12">4/17/04</span>
<li><span id="PPN13">Problem 1 (C5-C6 myelopathy): T=100, P=100, R=24. Treated with Ketamine CRI at 0.4 mg/kg/hr, morphine 0.3mg/kg/hr and Acepromazine 0.02 mg/mg q2hr overnight. LRS @120ml/kg/hr, decreased to 80ml/hr in evening due to UOP increase to 3.5ml/kg/hour. Given dexamethasone SP 0.1mg/kg IV at 2pm. A: Appears to be painful, but hard to assess due to level of sedation and dysphoria. UOP is adequate so decreased UOP last night may have been due to dehydration. Dex SP given as anti-inflammatory to help with potential inflammation surrounding her surgery site. P: Continue to monitor pain and treat with Ketamine CRI at 0.3 mg/kg/hr, morphine 0.3mg/kg/hr and Acepromazine 0.02 mg/mg q2hr. Keep quiet and comfortable. Continue LRS @120ml/hr. Monitor UOP</span>
<li><span id="PPN14">Problem 3 (UTI):T=100. Given ampicillin 22mg/kg. Urinary catheter placed post-op. UOP=0.6ml/kg/hr. Given lasix 0.2mg/kg once at 4pm. Given ampicillin 22mg/kg TID. A: Stable. UOP low, so given lasix. Low UOP could be due to renal insufficiency or dehydration. P: Monitor urine output. Urinary catheter care. Continue fluid therapy and ampicillin as above.</span>
<li><span id="PPN15">4/18/04</span>
<li><span id="PPN16">Problem 1 (C5-C6 myelopathy): T=100.1, P=100, R=24. Moved to wards in AM. Appears painful and dysphoric. Very sensitive to touch. Treated with Acepromazine 0.4mg at 9am. Given 2mg hydromorphone at 10:30am. Continue ampicillin 22mg/kg, LRS @60ml/kg/hr. A: Stable, but appears dysphoric. P: Discontinue morphine and monitor for pain and agitation. Give 0.05mg/kg hydromorphone PRN q6hrs. Give acepromazine 0.4 mg PRN q2hrs.</span>
<li><span id="PPN17">Problem 3 (UTI): T=100.1. Urine is discolored-brown/red. UOP= 1.4ml/kg/hr. Given Ampicillin 22mg/kg TID. A: Stable. Hematuria may be due to urinary catheter irritation or cystitis and less likely urolithiasis, neoplasia, coagulopathy, vasculitis, benign renal hematuria. Must differentiate from myoglobiuria, vaginal bleeding. UOP adequate. P: Continue as above with urinary catheter care, UOP measurement, fluid therapy and ampicillin 22mg/kg TID.</span>
<li><span id="PPN18">4/20/04</span>
<li><span id="PPN19">Problem 1 (C5-C6 myelopathy): T 102.5 (rechecked as 101.6), P 96, R 80. Sensitive to touch. Receiving Prednisone 10mg PO BID. Eating well when offered by hand. Incision is clean and dry, minimal swelling/erythema. Turning self. A: Stable. Continuing to improve. P: Continue with Prednisone 10mg PO BID, feed BID, water always. Monitor tonight. Gurney out in am.</span>
<li><span id="PPN20">Problem 3 (UTI): Emptied 1300ml yellow urine from urinary catheter collection this am. Removed urinary catheter this am. Stood and urinated on own in pm. Changed abx to Clavamox 750mg PO BID. A: Hematuria seems to have resolved. P: Continue with clavamox, monitor UOP.</span>
<li><span id="PPN21">4/21/04</span>
<li><span id="PPN22">Problem 1 (C5-C6 myelopathy): T 101.2, P 120, panting. Standing and turning on own. Gurneyed outside today, walked a short distance with a harness. Incision appears clean and dry. Murphy continues to be nervous but rests comfortably when alone. Continuing to give Prednisone 10mg PO BID. Eating and drinking well. A: Stable. Continuing to improve. P: Continue as above.</span>
<li><span id="PPN23">Problem 3 (UTI): Urinating well on own. Receiving Clavamox 750mg PO BID. A: Stable. P: Continue with meds.</span>
<li><span id="PPN24">4/22/04</span>
<li><span id="PPN25">Problem 1 (C5-C6 myelopathy): T 101.2, P 92, R 80. Eating well. Normal urination and BM. Stands when coaxed and occasionally cries out when first attempting to stand. Receiving Prednisone 10mg PO BID. Bx results revealed undifferentiated neoplasia - special stains pending. A: Continues to improve. P: Continue as above.</span>
<li><span id="PPN26">Problem 3 (UTI): Urinating normal amount on own, normal color, no stranguria. Receiving Clavamox 750mg PO BID. A: Stable. P: Continue as above.</span>
<li><span id="PPN27">4/23/04</span>
<li><span id="PPN28">Problem 1 (C5-C6 myelopathy): O: BAR. Eating and drinking well. Eager to stand and go out today. Urinating on own outside. Soft stools. Giving Pred 10mg PO BID. A: Continuing to improve. P: Discharge today.</span>
<li><span id="PPN29">Problem 3 (UTI): O: Receiving Clavamox 750mg PO TID. A: Stable. P: Continue on Clavamox 750mg PO TID. Recheck urine culture in 2-3 weeks. Continue on meds for 4-5 weeks. Follow up recheck 1 week after completing abx. Discharge today.</span>
</ol></div>

<h3><a name=C>Comments</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="C_content"><ul>
<li><span id="C1">cell 530-263-4525</span>
</ul></div>

<h3><a name=DS>Discharge Summary</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="DS_content"><ol>
<li><span id="DS1">Murphy presented to the UC Davis VMTH Neurology and Neurosurgery service on 4/14/04 for evaluation of a right forelimb lameness. A C1-T2 spinal cord lesion was suspected based on neurologic examination. On pre-anesthetic laboratory tests we found that Murphy had a urinary tract infection and evidence of pyelonephritis based on abdominal ultrasound findings (antibiotic therapy was initiated). She was otherwise healthy for anesthesia.</span>
<li><span id="DS2">On 4/15/04 she was put under anesthesia for further evaluation. Myelogram revealed an extradural mass dorsal to her cervical spine over the C5-C6 disk space. On 4/16/04 a dorsal laminectomy was performed to remove the mass. The mass was compressing her spinal cord and involving her dorsal nerve root from C5. The mass was submitted for histopathology (special stains are pending 4/23/04). Murphy was recovered in ICU. She was painful and agitated following surgery and was controlled with Morphine, Acepromazine, and a Ketamine CRI.</span>
<li><span id="DS3">Murphy returned to the wards on 4/18/04. She was treated with Prednisone 10mg PO BID for post-surgical inflammation and Clavamox 750mg PO BID for the UTI. She recovered well over the next few days and by 4/20/04 she was standing on her own and able to go outside to urinate and defecate.</span>
<li><span id="DS4">She improved daily and was discharged on 4/23/04. The owner was instructed to keep Murphy confined for next 4-6 weeks with very short walks outside daily. She will continue receiving Prednisone 10mg PO BID until the histopathology is complete. She will receive Clavamox 750mg PO TID for 4 more weeks. A culture should be performed in 2-3 weeks, and again 1 week after completion of the antibiotics. Further treatment for the possible neoplasia will be determined based on the completed histopathology.</span>
</ol></div>

<h3><a name=DI>Discharge Instructions</a> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></h3>
<div id="DI_content"><ol>
<li><span id="DI1">Thank you for bringing Murphy to the UC Davis, VMTH Neurology and Neurosurgery service! She is such a sweet girl. Murphy presented on 4/14/04 for evaluation of a right forelimb lameness. A cervical spinal cord lesion was suspected based on neurologic examination. On pre-anesthetic laboratory tests we found that Murphy had a urinary tract infection which was probably involving her kidneys. She was otherwise healthy for anesthesia. On 4/15/04 she was put under anesthesia for further evaluation. We found an extradural mass dorsal to her cervical spine over the C5-C6 disk space. The following day we did a dorsal laminectomy to remove the mass. The mass was compressing her spinal cord and involving her dorsal nerve root from C5. The mass was submitted for histopathology and at this point we know that it is an undifferentiated neoplasm - cell type is still pending special stains. She recovered well from surgery and has been improving every day.</span>
<li><span id="DI2">Please follow the instructions below for at home care:</span>
<li><span id="DI3">ACTIVITY: Please keep Murphy confined for the next 4-6 weeks. She has been in a 6'x3' run here and has been very comfortable. She should be able to stand and turn comfortably but not have much activity. She can take very short leash walks to urinate and defecate. As she improves she will want to be more active - please do not increase her activity for at least 4-6 weeks even if she feels good.</span>
<li><span id="DI4">HARNESS: Please use the harness and leash to take Murphy on walks. Do not use a collar around her neck.</span>
<li><span id="DI5">MEDICATIONS: Clavamox (antibiotic for the urinary tract infection) - Give 750mg orally three times daily for 4-6 weeks. The urine should be cultured in 3 weeks to make sure the infection is cleared. You can have your regular veterinarian submit the culture or you are welcome to have us perform the culture if Murphy is at the VMTH for radiation therapy. The urine should be cultured again one week after completing the antibiotics.</span>
<li><span id="DI6">Prednisone - Please give 10mg orally twice daily. Murphy has been very thirsty while on this medication - please make sure she has constant access to water.</span>
<li><span id="DI7">RECHECK: We will contact you when we have the results of the histopathology. At that point we will let you know what we recommend and we can schedule the necessary appointments at that time. Please call us if there are any problems or concerns or if you feel she is painful or deteriorating in any way.</span>
<li><span id="DI8">Murphy has been a great patient. Please do not hesitate to call should you have any questions or concerns (530) 752-1393 after hours/weekends 530-752-0186.</span>
<li><span id="DI9">Clinician: Dr. Stephanie Kube</span>
<li><span id="DI10">Student: Dani Rabwin and Karen Chase</span>
  </ol></div></div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>