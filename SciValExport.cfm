<cfset EISObj = createObject("Component","C.Personnel.EIS")>

<cfquery datasource="SQLSRV03-AAUD" name="q">
SELECT     MIN(e.emp_term_code) AS emp_term_code, e.emp_clientid, p.person_last_name, p.person_first_name, isnull(left(person_middle_name,1),'') as MidI,ids_MIV_ID,ids_mailid,ids_employee_id,
                          (SELECT     UCDHomeDept_dept_name
                            FROM          dictionary.dbo.UCDHomeDept
                            WHERE      (UCDHomeDept_dept_no = e.emp_home_dept)) AS home_dept
FROM         employees AS e INNER JOIN
                      dbo.person AS p ON e.emp_clientid = p.person_clientid
                      INNER JOIN ids on ids_pKey = person_pkey and ids_term_code >= 201401
WHERE     e.emp_term_code > 201401
			AND (e.emp_primary_title IN ('1450', '1451', '1452', '1895', '1897', '1898', '1899', '1900', '1901', '1902', '1904', '1905', '1906', '1908', '1909', '1910', '3001', '3009', '3011', '3019', '3021', '3029', '3031', '3032', '3206', 
                      '3216', '3226') 
				OR e.emp_effort_title_code IN ('1450', '1451', '1452', '1895', '1897', '1898', '1899', '1900', '1901', '1902', '1904', '1905', '1906', '1908', '1909', '1910', '3001', '3009', '3011', '3019', '3021', '3029', '3031', '3032', '3206', 
                      '3216', '3226')
                OR e.emp_teaching_title_code IN ('1450', '1451', '1452', '1895', '1897', '1898', '1899', '1900', '1901', '1902', '1904', '1905', '1906', '1908', '1909', '1910', '3001', '3009', '3011', '3019', '3021', '3029', '3031', '3032', 
                      '3206', '3216', '3226'))
GROUP BY e.emp_clientid, p.person_last_name, p.person_first_name, e.emp_home_dept, isnull(left(person_middle_name,1),''),ids_miv_id,ids_mailid,ids_employee_id
ORDER BY p.person_last_name, e.emp_home_dept
</cfquery>

<table>
  <tr>
  	<th>FirstName</th>
    <th>LastName</th>
    <th>MiddleInitial</th>
    <th>Degree1</th>
    <th>Degree2</th>
    <th>Department</th>
    <th>Department2</th>
    <th>Division</th>
    <th>Division2</th>
    <th>Date of Hire</th>
    <th>Email</th>
    <th>Email2</th>
    <th>Full Name</th>
    <th>Rank</th>
    <th>Series</th>
  </tr>
	<cfoutput query="q">
    <tr>
      <th>#q.person_First_Name#</th>
      <th>#q.person_last_name#</th>
      <th>#q.MidI#</th>

     	<cfset qDegrees = EISObj.getPersonMIVDegrees(q.ids_miv_id)>
      <cfquery dbtype="query" name="qDegreesOrdered">
      	SELECT Degree
        FROM qDegrees
        WHERE Degree != ''
        	AND Degree != 'Residency'
        ORDER BY EndDate DESC
      </cfquery>
           
      <th>#qDegreesOrdered.Degree[1]#</th>
      <th>#qDegreesOrdered.Degree[2]#</th>
      
      <th>#EISObj.getPersonDepartment(q.ids_employee_id,'HOME_DEPT')#</th>
      <th>#EISObj.getPersonDepartment(q.ids_employee_id,'ALT_DEPT_CD')#</th>
      <th></th>
      <th></th>
      <th>#EISObj.getPersonHireDate(q.ids_employee_id)#</th>
      <th>#q.ids_MailID#@ucdavis.edu</th>
      <th></th>
      <th>#q.person_First_Name# #q.person_last_name#</th>
      <th>#EISObj.getPersonJobGroup(q.ids_employee_id)#</th>
      <th>#EISObj.getPersonFacultyProgram(q.ids_employee_id)#</th>
    </tr>
	</cfoutput>
</table>