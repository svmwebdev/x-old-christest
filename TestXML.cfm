<cfif IsDefined("URL.Server")>
	<cfswitch expression="#URL.Server#" >
		<cfcase value="Train" >
			<cfset ServerURL = 'https://vmacs-train.vetmed.ucdavis.edu'>
		</cfcase>
        <cfcase value="NewProd" >
			<cfset ServerURL = 'https://vmacs-vmth.vetmed.ucdavis.edu'>
		</cfcase>
		<cfdefaultcase>
			<h1>Invalid Server Selected</h1>
			<cfabort>
		</cfdefaultcase>
	</cfswitch>

<h1>Validating against <cfoutput>#ServerURL#</cfoutput></h1>

<cfinclude template="include_VMACSArrayHandling.cfm" >

<h2>Client File</h2>
<cfhttp url="#ServerURL#/trust/query.xml?dbfile=103&index=Phone4&find=2097&format=FIVE9&limit=20&AUTH=06232005&filter=%28HomePhn[%22000%20000%202097%22%29!%28WorkPhn[%22000%20000%202097%22%29!%28FaxMssPh[%22000%20000%202097%22%29">
<cfset qResponse = parseXML(cfhttp.fileContent)>
Returned <cfoutput>#arrayLen(qResponse)# records</cfoutput>

<h2>RDVM File</h2>
<cfhttp url="#ServerURL#/trust/query.xml?dbfile=203&AUTH=06232005&index=Phone4&find=6684&format=FIVE9&limit=20&filter=%28WorkPhn[%22510%20548%206684%22%29!%28ScndPhn[%22510%20548%206684%22%29">
<cfset qResponse = parseXML(cfhttp.fileContent)>
Returned <cfoutput>#arrayLen(qResponse)# records</cfoutput>
 
<h2>Users File (user.xml)</h2>
<cfhttp url="#ServerURL#/trust/user.xml?AUTH=06232005&format=VCARS&filter=$$ucase^%25zString%28Name%29[%22THEON%22,$$active^htUser%28UserNo%29,%22LXV%22%27[Status">
<cfset qResponse = parseXML(cfhttp.fileContent)>
Returned <cfoutput>#arrayLen(qResponse)# records</cfoutput>

<h2>Users File (query.xml)</h2>
<cfhttp url="#ServerURL#/trust/query.xml?dbfile=3&index=CampusLoginId&find=phantem&format=CHRIS4&AUTH=06232005">
<cfset qResponse = parseXML(cfhttp.fileContent)>
Returned <cfoutput>#arrayLen(qResponse)# records</cfoutput>

<!---<h2>Visits File - Range</h2>
<cfhttp url="#ServerURL#/trust/query.js?dbfile=405&index=AdmsDate&find=12Aug15:17Aug15&filter=Student%28%22VmthPrsn%22%2C%22CampusLoginId%22%29%3D%22abrus%22&format=CTS&limit=200&AUTH=06232005">
<cfset qResponse = parseXML(cfhttp.fileContent)>
Returned <cfoutput>#arrayLen(qResponse)# records</cfoutput>
--->
<h2>Visits File - Specific</h2>
<cfhttp url="#ServerURL#/trust/query.xml?dbfile=405&index=VisitNo&find=72153EM&format=BRANDT&limit=200&AUTH=06232005">
<cfset qResponse = parseXML(cfhttp.fileContent)>
Returned <cfoutput>#arrayLen(qResponse)# records</cfoutput>

<h2>Procedures File</h2>
<cfhttp url="#ServerURL#/trust/query.xml?dbfile=409&index=Visit.VisitNo&find=72153EM&format=CTS&AUTH=06232005">
<cfset qResponse = parseXML(cfhttp.fileContent)>
Returned <cfoutput>#arrayLen(qResponse)# records</cfoutput>
 
<h2>Visits File</h2>
<cfhttp url="#ServerURL#/trust/query.xml?dbfile=405&index=VisitNo&find=72153EM&format=BRANDT&limit=200&AUTH=06232005">
<cfset qResponse = parseXML(cfhttp.fileContent)>
Returned <cfoutput>#arrayLen(qResponse)# records</cfoutput>


 <cfabort>
 
GET /trust/query.xml?dbfile=401&index=AcctPrcd&format=CTS&limit=9999&AUTH=06232005
 https://vmacsdev.vmth.ucdavis.edu/trust/query.xml?dbfile=401&index=AcctPrcd&format=CTS&limit=9999&AUTH=06232005
<cfelse>
<h1>Select A Server</h1>

<ul>
	<li><a href="TestXML.cfm?Server=Train">VMACS-TRAIN.VETMED</a></li>
    <li><a href="TestXML.cfm?Server=NewProd">VMACS-VMTH.VETMED.UCDAVIS.EDU</a></li>
</ul>
</cfif>