<cfsetting requestTimeOut = "1200">

<cfinclude template="QueryToCSV.cfm">
<cfset maxRows = 100000>
  
<!--- RecoveryPACS StartDate = 9/26/2001 --->
<!--- RecoveryPACS EndDate = 9/26/2001 --->
<cfset TrueStartDate = createDate(2005,1,1)>
<cfset TrueEndDate = createDate(2005,1,5)>
  
<cfset StartDate = #DateFormat(TrueStartDate,'yyyy-mm-dd')#>
<cfset EndDate = #DateFormat(TrueEndDate,'yyyy-mm-dd')#>
  
<cfset includedProcedures = "">

<cfset includedProcedures = listAppend(includedProcedures,"'6016'")> <!--- SA Thorax --->
<cfset includedProcedures = listAppend(includedProcedures,"'6900'")> <!--- SA Ultrasonography Scan --->
  
<!--- REPORTS --->
<cfset modalityList = 'Sa_Radiography,Ultrasonography'>

<cfset qReport = queryNew("PatientId,InvoiceID,ProcID,ProcDate,Body,Status,Modality","integer,varchar,varchar,varchar,varchar,varchar,varchar")>

  REPORTS
  
  <cfset lastRptRecordCount = 0>
    
  <cfloop list="#modalityList#" index="currentModality">
    <br><cfoutput>#currentModality#</cfoutput><cfflush>

      <cfswitch expression="#currentModality#">
        <cfcase value="Sa_Radiography">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">          
            
                      SELECT PatientNbr, visit_invoice.Invoice, #currentModality#.Name as PatientName, Request, #currentModality#_Study_Def.name as ReportSection, 
            Significant_Findings, '' as Overflow, Date_Of_Request, 'DX' as Modality, #currentModality#.Report_Status,
            Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq
          FROM VMACS.#currentModality#
              INNER JOIN VMACS.#currentModality#_Report_Summary on #currentModality#.IEN = #currentModality#_Report_Summary.#currentModality#
              INNER JOIN VMACS.#currentModality#_Study_Def ON #currentModality#_Study_Def.IEN = #currentModality#_Report_Summary.IEN
              INNER JOIN VMACS.#currentModality#_Procedure_Requested SARAD_PR ON SARAD_PR.#currentModality# = #currentModality#.IEN
              INNER JOIN VMACS.Vmth_Patient p ON p.IEN = #currentModality#.Patient
              INNER JOIN VMACS.Visit_Invoice on visit_invoice.IEN = #currentModality#.Invoice
          where 1=1
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested IN (#PreserveSingleQuotes(IncludedProcedures)#)
          order by #currentModality#_Report_Summary.#currentModality#, procedure_requested, #currentModality#_Study_Def.IEN
            
            
          </cfquery>
        </cfcase>
        
        
        <cfcase value="Ultrasonography">
          <cfquery datasource="VMACS-Prod" name="qRpt" maxrows="#maxRows#">
                      SELECT PatientNbr, visit_invoice.Invoice, #currentModality#.Name as PatientName, Request, Ultrasound_Study_Def.name as ReportSection, 
            Significant_Findings, '' as Overflow, Date_Of_Request, 'US' as Modality, #currentModality#.Report_Status,
            Procedure_Requested, {fn CONCAT({fn CONCAT(request,'^')},Procedure_Requested)} as procreq
          FROM VMACS.#currentModality#
              INNER JOIN VMACS.#currentModality#_Report_Summary on #currentModality#.IEN = #currentModality#_Report_Summary.#currentModality#
              INNER JOIN VMACS.Ultrasound_Study_Def ON Ultrasound_Study_Def.IEN = #currentModality#_Report_Summary.IEN
              INNER JOIN VMACS.#currentModality#_Procedure_Requested SARAD_PR ON SARAD_PR.#currentModality# = #currentModality#.IEN
              INNER JOIN VMACS.Vmth_Patient p ON p.IEN = #currentModality#.Patient
              INNER JOIN VMACS.Visit_Invoice on visit_invoice.IEN = #currentModality#.Invoice
          where 1=1
            AND PatientNbr is not null
            AND Date_Of_Request between '#StartDate#' AND '#EndDate#'
            AND Procedure_Requested IN (#PreserveSingleQuotes(IncludedProcedures)#)
          order by #currentModality#_Report_Summary.#currentModality#, procedure_requested, Ultrasound_Study_Def.IEN
          </cfquery>
        </cfcase>
        
    
    </cfswitch>
    
<cfoutput>Total #qRpt.recordcount# rows<br></cfoutput>

<cfoutput query="qRpt" group="procReq">
  <cfset queryAddRow(qReport)>
  <cfset querySetCell(qReport, "patientid", qRpt.PatientNbr)>
  <cfset querySetCell(qReport, "InvoiceID", qRpt.Invoice)>
  <cfset querySetCell(qReport, "ProcID", qRpt.Procedure_Requested)>
  <cfset querySetCell(qReport, "ProcDate", DateFormat(createODBCDate(qRpt.Date_Of_Request),'yyyymmdd'))>
  <cfif len(qRpt.Report_Status)>
    <cfswitch expression="#qRpt.Report_Status#">
      <cfcase value="DRAFT,PRELIMINARY"> 
        <cfset querySetCell(qReport, "Status", "P")>
      </cfcase>
      <cfcase value="FINAL">
        <cfset querySetCell(qReport, "Status", "F")>
      </cfcase>
      <cfdefaultcase>
        <h1>Error with report status (#qRpt.Report_Status#)- #qRpt.Modality##qRpt.Request##qRpt.Procedure_Requested#</h1> 
        <cfabort>
      </cfdefaultcase>
    </cfswitch>
  <cfelse> <!--- Some draft status reports have no value in status --->
    <cfset querySetCell(qReport, "Status", "P")>
  </cfif>
  <cfset querySetCell(qReport, "Modality", qRpt.Modality)>

  <cfset Body = ''>
  <cfoutput>  
    <cfset theSigFindings = replace(trim(qRpt.Significant_Findings),'#chr(13)##chr(10)#','\.br\\.br\', 'all')>
    <cfset theOverflow = replace(trim(qRpt.Overflow),'#chr(13)##chr(10)#','\.br\\.br\', 'all')>
    <cfset Body = Body & '\.br\\.br\' & qRpt.ReportSection & '\.br\\.br\' & theSigFindings & theOverflow & '\.br\\.br\'>
    <cfif qRpt.currentRow%100 eq 0>#NumberFormat(qRpt.currentRow/qRpt.recordcount*100,'9.9')#% * <cfflush></cfif>
  </cfoutput>
  <cfset querySetCell(qReport, "Body", trim(body))>


</cfoutput>
<cfoutput><br>Total #qReport.recordcount-lastRptRecordCount# records<br></cfoutput>
<cfset lastRptRecordCount = qReport.recordcount>
      

</cfloop> 
      

<cfscript> 
  ///We need an absolute path, so get the current directory path. 
    theFile=GetDirectoryFromPath(GetCurrentTemplatePath()) & "VMACSExportKrystle.xls"; 

  //Create a new Excel spreadsheet object and add the query data. 
  theSpreadsheet = SpreadsheetNew("Report"); 
  SpreadsheetAddRow(theSpreadsheet,"PatientId,InvoiceID,ProcID,ProcDate,Body,Status,Modality");
  SpreadsheetAddRows(theSpreadsheet,qReport);
</cfscript>       
 
<!--- Write the spreadsheet to a file, replacing any existing file. ---> 
<cfspreadsheet action="write" filename="#theFile#" name="theSpreadsheet" overwrite=true>

  <h1>DONE</h1>