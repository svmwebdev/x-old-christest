<cfset portalDetails = new c.vmacs.portalDetails() />
<cfset portalDetails.copyAllData() />
<cfdump var="#portalDetails.getResults()#" />
<cfabort />

<cfset VMACSDSN = 'VMACSDirect'>
<cfset VHISDSN = 'VHIS-Dev'>
  
<cfset VMACSDSN = 'VMACS-Prod'>
<cfset VHISDSN = 'VHIS-Prod'>

<!--- CLIENT --->
<cfquery datasource="#VMACSDSN#" name="qClientPortal">
  select v.Client as ClientID, v.Name as ClientName, Last_Modified
  from VMACS.Client_Portal_Event_Log e 
  inner join VMACS.Client_Portal_Event_Log_Type t on e.Client_Portal_Event_Log_Type_Id = t.Id
  inner join VMACS.Client_Portal_User u on u.Id = e.Client_Portal_User_Id
  inner join VMACS.Vmth_Client v on u.Vmth_Client_Id = v.IEN
  inner join VMACS.Vmth_Person p on e.Last_Modified_By = p.IEN
  WHERE (t.name = 'login')
         AND v.Client != 'B28025' <!--- Brandt --->
  order by Last_Modified
</cfquery>
 
Client Logins: <cfoutput>#qClientPortal.recordcount#</cfoutput><br>

<cfquery datasource="#VHISDSN#">
  DELETE FROM ClientPortalLogins
</cfquery>

<cfloop query="qClientPortal">
  <cfquery datasource="#VHISDSN#">
    INSERT INTO ClientPortalLogins(ClientID,ClientName,LastModified)
    VALUES('#qClientPortal.ClientID#','#qClientPortal.ClientName#',
    '#qClientPortal.Last_Modified#')
  </cfquery>
</cfloop>
  
<cfquery datasource="#VMACSDSN#" name="qClientRegCount">
  SELECT count(rowid) as ClientPortalRegCount
  FROM VMACS.Client_Portal_User
</cfquery>
  
<cfquery datasource="#VHISDSN#">
  UPDATE PortalAccountCounts
  SET ClientAccounts = #qClientRegCount.ClientPortalRegCount#
</cfquery>
  
<!--- Transactions --->
<cfquery datasource="#VMACSDSN#" name="qTransactions">
  SELECT client_portal_user_id, Created_Date, Amount
  FROM VMACS.Client_Portal_Transaction
  where status = 'success'
</cfquery> 
  
<cfquery datasource="#VHISDSN#">
  DELETE FROM ClientPortalTransactions
</cfquery>

<cfloop query="qTransactions">
  <cfquery datasource="#VHISDSN#">
    INSERT INTO ClientPortalTransactions(TransDate,ClientID,Amount)
    VALUES('#qTransactions.Created_Date#','#qTransactions.client_portal_user_id#',#qTransactions.amount#)
  </cfquery>
</cfloop>  

<cfoutput>
  RegCount: #qClientRegCount.ClientPortalRegCount#<br>
  Transaction Count: #qTransactions.recordcount#<br>
</cfoutput>
  
<!--- REFVET --->
<cfquery datasource="#VMACSDSN#" name="qRefVetPortal">
  select u.Id as RefVetID, e.Last_Modified, u.Name as RefVetName
  from VMACS.Rdvm_Portal_Event_Log e 
  inner join VMACS.Client_Portal_Event_Log_Type t on e.Rdvm_Portal_Event_Log_Type_Id = t.Id
  inner join VMACS.Rdvm_Person u on u.Id = e.Rdvm_Person_Id--inner join VMACS.Vmth_Person p on e.Last_Modified_By = p.IEN
  WHERE t.Name = 'login'
  and u.id != 12910 -- Mike Barlia
  and u.id != 15731 -- karen Vernau
  order by u.id
  </cfquery>

<cfquery datasource="#VHISDSN#">
  DELETE FROM RefVetPortalLogins
</cfquery>

<cfloop query="qRefVetPortal">
  <cfquery datasource="#VHISDSN#">
    INSERT INTO RefVetPortalLogins(RefVetID,RefVetName,LastModified)
    VALUES('#qRefVetPortal.RefVetID#','#qRefVetPortal.RefVetName#',
    '#qRefVetPortal.Last_Modified#')
  </cfquery>
</cfloop>
  
<cfquery datasource="#VMACSDSN#" name="qRefVetRegCount">
  SELECT count(rowid) as RDVMRegCount
  FROM VMACS.Rdvm_Person
  where registered = 1
</cfquery>
  
<cfquery datasource="#VHISDSN#">
  UPDATE PortalAccountCounts
  SET RefVetAccounts = #qRefVetRegCount.RDVMRegCount#
</cfquery>

<!--- MESSAGES --->
<cfquery datasource="#VMACSDSN#" name="qMessages">
  SELECT Contacted_Date, ToFrom, Communication_Type
  FROM VMACS.Messages
  where Communication_Type = 'CLIENT_PORTAL' OR Communication_Type = 'REFVET_PORTAL'
</cfquery>

Messages: <cfoutput>#qMessages.recordcount#</cfoutput><br>

<cfquery datasource="#VHISDSN#">
  DELETE FROM PortalMessages
</cfquery>

<cfloop query="qMessages">
  <cfquery datasource="#VHISDSN#">
    INSERT INTO PortalMessages(Contacted_Date,ToFrom,Portal)
    VALUES('#qMessages.Contacted_Date#','#qMessages.ToFrom#', '#qMessages.Communication_Type#')
  </cfquery>
</cfloop>
